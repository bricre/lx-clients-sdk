<?php

namespace LogisticsX\Clients;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public array $types = [
        'getClientCollection' => [
            '200.' => 'LogisticsX\\Clients\\Model\\Client\\Read[]',
        ],
        'postClientCollection' => [
            '201.' => 'LogisticsX\\Clients\\Model\\Client\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'statusStatisticsClientCollection' => [
            '200.' => 'LogisticsX\\Clients\\Model\\Client\\Statistics[]',
        ],
        'getClientItem' => [
            '200.' => 'LogisticsX\\Clients\\Model\\Client\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putClientItem' => [
            '200.' => 'LogisticsX\\Clients\\Model\\Client\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteClientItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getVatCollection' => [
            '200.' => 'LogisticsX\\Clients\\Model\\Vat\\Read[]',
        ],
        'postVatCollection' => [
            '201.' => 'LogisticsX\\Clients\\Model\\Vat\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'statusStatisticsVatCollection' => [
            '200.' => 'LogisticsX\\Clients\\Model\\Vat\\Statistics[]',
        ],
        'getVatItem' => [
            '200.' => 'LogisticsX\\Clients\\Model\\Vat\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putVatItem' => [
            '200.' => 'LogisticsX\\Clients\\Model\\Vat\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteVatItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
    ];
}
