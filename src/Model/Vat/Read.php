<?php

namespace LogisticsX\Clients\Model\Vat;

use OpenAPI\Runtime\AbstractModel;

class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $vat = null;

    /**
     * @var string
     */
    public $eori = null;

    /**
     * @var string
     */
    public $businessName = null;

    /**
     * @var string|null
     */
    public $addressLine1 = null;

    /**
     * @var string|null
     */
    public $addressLine2 = null;

    /**
     * @var string|null
     */
    public $addressLine3 = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string|null
     */
    public $postCode = null;

    /**
     * @var string|null
     */
    public $countryIso = null;

    /**
     * @var int
     */
    public $isForPva = null;

    /**
     * @var string
     */
    public $status = 'UNVERIFIED';

    public $client = null;

    /**
     * @var string
     */
    public $fullAddress = null;
}
