<?php

namespace LogisticsX\Clients\Model\Client;

use OpenAPI\Runtime\AbstractModel;

class Write extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string
     */
    public $paymentTerm = 'POSTPAID';

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    public $parent = null;
}
