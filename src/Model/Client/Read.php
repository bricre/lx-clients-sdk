<?php

namespace LogisticsX\Clients\Model\Client;

use OpenAPI\Runtime\AbstractModel;

class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $uuid = null;

    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string
     */
    public $paymentTerm = 'POSTPAID';

    /**
     * @var string[]
     */
    public $config = null;

    /**
     * @var string
     */
    public $createTime = null;

    /**
     * @var string|null
     */
    public $updateTime = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    public $parent = null;
}
