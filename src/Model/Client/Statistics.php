<?php

namespace LogisticsX\Clients\Model\Client;

use OpenAPI\Runtime\AbstractModel;

class Statistics extends AbstractModel
{
    /**
     * @var string
     */
    public $statisticsBy = null;

    /**
     * @var int
     */
    public $statisticsCount = null;
}
