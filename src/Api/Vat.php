<?php

namespace LogisticsX\Clients\Api;

use LogisticsX\Clients\Model\Vat\Read;
use LogisticsX\Clients\Model\Vat\Statistics;
use LogisticsX\Clients\Model\Vat\Write;

class Vat extends AbstractAPI
{
    /**
     * Retrieves the collection of Vat resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'vat'	string
     *                       'eori'	string
     *                       'client.code'	string
     *                       'client.code[]'	array
     *                       'client.id'	integer
     *                       'client.id[]'	array
     *                       'status'	string
     *                       'status[]'	array
     *                       'isForPva'	integer
     *                       'isForPva[]'	array
     *                       'businessName'	string
     *                       'order[id]'	string
     *                       'order[vat]'	string
     *                       'order[eori]'	string
     *                       'order[client.code]'	string
     *                       'order[status]'	string
     *                       'order[isForPva]'	string
     *                       'order[businessName]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getVatCollection',
        'GET',
        'api/clients/vats',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Vat resource.
     *
     * @param Write $Model The new Vat resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postVatCollection',
        'POST',
        'api/clients/vats',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves the collection of Vat resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'vat'	string
     *                       'eori'	string
     *                       'client.code'	string
     *                       'client.code[]'	array
     *                       'client.id'	integer
     *                       'client.id[]'	array
     *                       'status'	string
     *                       'status[]'	array
     *                       'isForPva'	integer
     *                       'isForPva[]'	array
     *                       'businessName'	string
     *                       'order[id]'	string
     *                       'order[vat]'	string
     *                       'order[eori]'	string
     *                       'order[client.code]'	string
     *                       'order[status]'	string
     *                       'order[isForPva]'	string
     *                       'order[businessName]'	string
     *
     * @return Statistics[]|null
     */
    public function statusStatisticsCollection(array $queries = []): ?array
    {
        return $this->request(
        'statusStatisticsVatCollection',
        'GET',
        'api/clients/vats/statistics',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a Vat resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $uuid): ?Read
    {
        return $this->request(
        'getVatItem',
        'GET',
        "api/clients/vats/$uuid",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Vat resource.
     *
     * @param string $uuid  Resource identifier
     * @param Write  $Model The updated Vat resource
     *
     * @return Read
     */
    public function putItem(string $uuid, Write $Model): Read
    {
        return $this->request(
        'putVatItem',
        'PUT',
        "api/clients/vats/$uuid",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Vat resource.
     *
     * @param string $uuid Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $uuid): mixed
    {
        return $this->request(
        'deleteVatItem',
        'DELETE',
        "api/clients/vats/$uuid",
        null,
        [],
        []
        );
    }
}
