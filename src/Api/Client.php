<?php

namespace LogisticsX\Clients\Api;

use LogisticsX\Clients\Model\Client\Read;
use LogisticsX\Clients\Model\Client\Statistics;
use LogisticsX\Clients\Model\Client\Write;

class Client extends AbstractAPI
{
    /**
     * Retrieves the collection of Client resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'name'	string
     *                       'email'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'telephone'	string
     *                       'telephone[]'	array
     *                       'parent.name'	string
     *                       'parent.code'	string
     *                       'paymentTerm'	string
     *                       'paymentTerm[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[code]'	string
     *                       'order[name]'	string
     *                       'order[email]'	string
     *                       'order[status]'	string
     *                       'order[telephone]'	string
     *                       'order[parent.name]'	string
     *                       'order[parent.code]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *                       'order[paymentTerm]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getClientCollection',
        'GET',
        'api/clients/clients',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Client resource.
     *
     * @param Write $Model The new Client resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postClientCollection',
        'POST',
        'api/clients/clients',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves the collection of Client resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'name'	string
     *                       'email'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'telephone'	string
     *                       'telephone[]'	array
     *                       'parent.name'	string
     *                       'parent.code'	string
     *                       'paymentTerm'	string
     *                       'paymentTerm[]'	array
     *                       'id'	integer
     *                       'id[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[code]'	string
     *                       'order[name]'	string
     *                       'order[email]'	string
     *                       'order[status]'	string
     *                       'order[telephone]'	string
     *                       'order[parent.name]'	string
     *                       'order[parent.code]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *                       'order[paymentTerm]'	string
     *
     * @return Statistics[]|null
     */
    public function statusStatisticsCollection(array $queries = []): ?array
    {
        return $this->request(
        'statusStatisticsClientCollection',
        'GET',
        'api/clients/clients/statistics',
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a Client resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getClientItem',
        'GET',
        "api/clients/clients/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Client resource.
     *
     * @param string $id    Resource identifier
     * @param Write  $Model The updated Client resource
     *
     * @return Read
     */
    public function putItem(string $id, Write $Model): Read
    {
        return $this->request(
        'putClientItem',
        'PUT',
        "api/clients/clients/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Client resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteClientItem',
        'DELETE',
        "api/clients/clients/$id",
        null,
        [],
        []
        );
    }
}
