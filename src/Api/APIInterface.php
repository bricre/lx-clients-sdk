<?php

namespace LogisticsX\Clients\Api;

use OpenAPI\Runtime\APIInterface as BaseClass;

interface APIInterface extends BaseClass
{
}
